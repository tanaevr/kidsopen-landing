<?php

/* @var $this yii\web\View */

$this->title = 'Kidsopen';

?>
<?php $this->beginContent('@app/views/layouts/header.php'); ?>

<?php $this->endContent(); ?>

<div class="container">
    <div class="row">
        <div class="col-lg-5 col-lg-offset-4 subscription">
            <div class="subscription_content">
                <p>Поддержите наш проект одним кликом узнайте первыми о его запуске!</p>
                <a href="#" class="btn btn-lg btn-warning subscription_btn">
                    Подписаться
                </a>
            </div>
            <div class="subscription_after serif">
                + и получите уникальное<br />
                <u>достижение «Самый первый»</u>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="prototype">
                <div class="prototype_count">
                    <span class="prototype_count_num">
                        145 267
                    </span>
                    <span class="prototype_count_text">
                        мест со всего мира<br/>уже в базе
                    </span>
                </div>
                <div class="prototype_pic">
                    <img src="img/prototype.png" alt="" />
                </div>
                
                <img src="img/prototype-shadow.png" class="prototype_pic_shadow" alt="" />
            </div>
            
        </div>
        <div class="col-lg-5 col-lg-offset-1 prototype-summary">
            <div class="row">
                <div class="col-lg-2">
                    <img src="img/number1.png" alt="" />
                </div>
                <div class="col-lg-10">
                    <div class="prototype-summary-title"><span>Поиск мест для детей во всех</span><br /><span>уголках мира</span></div>
                    <div class="prototype-summary-info">
                        <p>На карте, рядом 
                        с вами или за 10000 км.</p>

                        <p>От России до Ганалулу 
                        одним кликом.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2">
                    <img src="img/number2.png" alt="" />
                </div>
                <div class="col-lg-10">
                    <div class="prototype-summary-title"><span>Знаем, что интересно именно</span><br /><span>вашим детям</span></div>
                    <div class="prototype-summary-info">
                        <p>Детализируем места 
                        по тематике, возрасту 
                        и еще более 
                        15 параметрам.</p>

                        <p>Нужны рыбки? 
                        Динозавры? Зарпосто!</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2">
                    <img src="img/number3.png" alt="" />
                </div>
                <div class="col-lg-10">
                    <div class="prototype-summary-title"><span>Экономим время родителей.</span><br /><span>Инструменты</span></div>
                    <div class="prototype-summary-info">
                        <p><a href="#">Выбрать интересный мне функционал</a></p>

                        <div class="_intro serif">Расскажите нам,<br />какие инструменты<br />Вам нужнее всего</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

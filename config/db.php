<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=kidsopen',
    'tablePrefix' => 'tbl_',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
];
